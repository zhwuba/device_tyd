
-include $(TOPDIR)vendor/*/build/core/definitions.mk

# $(1): argument key
# $(2): argument values
# $(3): module list
define tyd-install-modules-vars
$(call install-modules-vars,TYD_PACKAGE_,$(1),$(2),$(3))
endef

################################ Common Configs ################################
# Device name [pqt18_gb -> pqt18_gb]
TYD_PRODUCT_DEVICE := $(TARGET_PRODUCT)
# Model name [pqt18_gb -> pqt18]
TYD_PRODUCT_MODEL := $(firstword $(subst _, ,$(TYD_PRODUCT_DEVICE)))
# Board name
TYD_PRODUCT_BOARD ?=
# Device custom home
TYD_PRODUCT_CUSTOM_HOME ?= tyd/$(TYD_PRODUCT_BOARD)/$(TYD_PRODUCT_MODEL)/$(TYD_PRODUCT_DEVICE)
# Device based platform
TYD_PRODUCT_PLATFORM ?= $(shell echo $(QCOM_PLATFORM) | tr '[A-Z]' '[a-z]')
# Screen attributions
TYD_SCREEN_WIDTH ?= $(LCM_WIDTH)
TYD_SCREEN_HEIGHT ?= $(LCM_HEIGHT)
# [hd720, etc]
TYD_LCD_RESOLUTION ?= $(BOOT_LOGO)

############################### Platform Configs ###############################

# TYD product info overrides
TYD_PRODUCT_BUILD_PROP_OVERRIDES := \
    $(if $(TYD_PRODUCT_INFO_FLAVOR),TARGET_BUILD_FLAVOR="$(TYD_PRODUCT_INFO_FLAVOR)") \
    $(if $(TYD_PRODUCT_INFO_DEVICE),TARGET_DEVICE="$(TYD_PRODUCT_INFO_DEVICE)") \
    $(if $(TYD_PRODUCT_INFO_NAME),PRODUCT_NAME="$(TYD_PRODUCT_INFO_NAME)") \
    $(if $(TYD_PRODUCT_INFO_BRAND),PRODUCT_BRAND="$(TYD_PRODUCT_INFO_BRAND)") \
    $(if $(TYD_PRODUCT_INFO_MODEL),PRODUCT_MODEL="$(TYD_PRODUCT_INFO_MODEL)") \
    $(if $(TYD_PRODUCT_INFO_MANUFACTURER),PRODUCT_MANUFACTURER="$(TYD_PRODUCT_INFO_MANUFACTURER)") \
    $(if $(TYD_PRODUCT_INFO_SW_VERNO),BUILD_DISPLAY_ID="$(TYD_PRODUCT_INFO_SW_VERNO)")
TYD_PRODUCT_VENDOR_BUILD_PROP_OVERRIDES := \
    $(if $(TYD_PRODUCT_INFO_DEVICE),TARGET_DEVICE="$(TYD_PRODUCT_INFO_DEVICE)") \
    $(if $(TYD_PRODUCT_INFO_NAME),PRODUCT_NAME="$(TYD_PRODUCT_INFO_NAME)") \
    $(if $(TYD_PRODUCT_INFO_BRAND),PRODUCT_BRAND="$(TYD_PRODUCT_INFO_BRAND)") \
    $(if $(TYD_PRODUCT_INFO_MODEL),PRODUCT_MODEL="$(TYD_PRODUCT_INFO_MODEL)") \
    $(if $(TYD_PRODUCT_INFO_MANUFACTURER),PRODUCT_MANUFACTURER="$(TYD_PRODUCT_INFO_MANUFACTURER)") \
    $(if $(TYD_PRODUCT_INFO_BOARD),TARGET_BOOTLOADER_BOARD_NAME="$(TYD_PRODUCT_INFO_BOARD)")


# BootAnimation
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/bootanimation/bootanimation.mk)
# Fonts
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/fonts/fonts.mk)
# Sounds
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/sounds/sounds.mk)
# Presets
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/presets/presets.mk)
# Prebuilts
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/prebuilts/prebuilts.mk)
# Wallpapers
$(call inherit-product-if-exists, vendor/tyd/frameworks/base/data/wallpapers/wallpapers.mk)
# Fingerprint
$(call inherit-product-if-exists, device/tyd/common/fingerprint/fingerprint.mk)

# TYD overlay (high<->low)
DEVICE_PACKAGE_OVERLAYS +=
PRODUCT_PACKAGE_OVERLAYS += \
    $(if $(filter user,$(TARGET_BUILD_VARIANT)),$(wildcard $(TYD_PRODUCT_CUSTOM_HOME)/overlay/user))  \
    $(wildcard $(TYD_PRODUCT_CUSTOM_HOME)/overlay) \
    $(if $(filter user,$(TARGET_BUILD_VARIANT)),$(wildcard device/tyd/$(TYD_PRODUCT_DEVICE)/overlay/user)) \
    $(wildcard device/tyd/$(TYD_PRODUCT_DEVICE)/overlay) \
    $(if $(filter user,$(TARGET_BUILD_VARIANT)),$(wildcard device/tyd/$(TYD_PRODUCT_PLATFORM)/overlay/user)) \
    $(wildcard device/tyd/$(TYD_PRODUCT_PLATFORM)/overlay) \
    $(if $(filter user,$(TARGET_BUILD_VARIANT)),$(wildcard device/tyd/common/overlay/user)) \
    device/tyd/common/overlay

# Freeme option
TYD_PRODUCT_OPTIONS := \
    device/tyd/common/ProjectOption.ini \
    $(wildcard device/tyd/$(TYD_PRODUCT_PLATFORM)/ProjectOption.ini) \
    $(wildcard device/tyd/$(TYD_PRODUCT_DEVICE)/ProjectOption.ini) \
    $(wildcard $(TYD_PRODUCT_CUSTOM_HOME)/ProjectOption.ini)

# Device Screen
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=$(TYD_DEVICE_LCD_DENSITY)

-include $(sort $(wildcard vendor/tyd/device/*/device.*.mk))

############################# Application & Feature ############################

# Auto generate wifi mac address
ifeq ($(strip $(TYD_MAC_AUTOGEN_SUPPORT)),yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.tyd.auto_generate_mac=1
endif

#Glove Mode
ifeq ($(strip $(TYD_GLOVE_MODE_SUPPORT)),yes)
  PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.glove_mode=1
  PRODUCT_PACKAGES += init.tyd.glovemode.rc
endif

# -- Applications

PRODUCT_PACKAGES += \
  $(call tyd-install-modules-vars,,, \
   ) \
  $(call tyd-install-modules-vars,TYD_PRODUCT_FOR,!factory, \
   )

# @} ###########################################################################


############################### 3rd Application ################################


