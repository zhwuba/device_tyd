#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#TOP_PATH := $(call my-dir)
# device path
MY_DEVICE_ROOTDIR := device/tyd/common/fingerprint/sunwave_ree

ifeq ($(strip $(TARGET_COPY_OUT_VENDOR)),)
TARGET_COPY_OUT_VENDOR := vendor
endif

PRODUCT_COPY_FILES += frameworks/native/data/etc/android.hardware.fingerprint.xml:system/etc/permissions/android.hardware.fingerprint.xml
PRODUCT_COPY_FILES += $(MY_DEVICE_ROOTDIR)/prebuilts/sf_ta:$(TARGET_COPY_OUT_VENDOR)/bin/sf_ta
PRODUCT_COPY_FILES += $(MY_DEVICE_ROOTDIR)/prebuilts/vendor.sw.swfingerprint@1.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.sw.swfingerprint@1.0-service.rc

PRODUCT_PACKAGES += vendor.sw.swfingerprint@1.0-service
PRODUCT_PACKAGES += vendor.sw.swfingerprint@1.0
PRODUCT_PACKAGES += vendor.sw.swfingerprint@1.0-impl
PRODUCT_PACKAGES += fingerprint.default
PRODUCT_PACKAGES += fingerprint.sunwave_ree

# sunwave sepolicy
BOARD_SEPOLICY_DIRS += $(MY_DEVICE_ROOTDIR)/sepolicy

# sunwave manifest.xml
DEVICE_MANIFEST_FILE += $(MY_DEVICE_ROOTDIR)/manifest.xml
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += $(MY_DEVICE_ROOTDIR)/compatibility_matrix.xml
