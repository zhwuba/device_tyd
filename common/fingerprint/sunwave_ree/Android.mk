LOCAL_PATH := $(call my-dir)

#############################################################################
# COMMON
#----------------------------------------------------------------------------
# We have to use BUILD_PREBUILT instead of PRODUCT_COPY_FILES,
# to copy over the NOTICE file.
#############################################################################
# For binary(executable or shared library).
# $(1): module
# $(2): suffix, [ .so ]
# $(3): class, [ EXECUTABLES, SHARED_LIBRARIES ]
# $(4): relative path, [ hw ]
# $(5): arch, [ 32, 64, both ]
# $(6): init-rc name
#----------------------------------------------------------------------------
define build-one-prebuilt-binary
include $$(CLEAR_VARS)
LOCAL_MODULE                := $(1)
LOCAL_MODULE_SUFFIX         := $(2)
LOCAL_MODULE_CLASS          := $(3)
LOCAL_MODULE_RELATIVE_PATH  := $(4)
LOCAL_MODULE_TAGS           := optional
LOCAL_SRC_FILES_arm64       := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/prebuilts/lib64/$(strip $(1))$(strip $(2))))
LOCAL_SRC_FILES_arm         := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/prebuilts/lib/$(strip $(1))$(strip $(2))))
LOCAL_MULTILIB              := $(5)
LOCAL_PROPRIETARY_MODULE    := true
LOCAL_INIT_RC               := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/prebuilts/$(strip $(6)).rc))
include $$(BUILD_PREBUILT)
endef

#############################################################################
# SAMPLE
#----------------------------------------------------------------------------
# $(eval $(call build-one-prebuilt-binary, libsample, .so, SHARED_LIBRARIES, , 32, ))
# $(eval $(call build-one-prebuilt-binary, sample.two, .so, SHARED_LIBRARIES, hw, 64, ))
# $(eval $(call build-one-prebuilt-binary, sample.three, .so, SHARED_LIBRARIES, hw, both, init.freeme.fingerprint.ext))
# $(eval $(call build-one-prebuilt-binary, sample, , EXECUTABLES, , 32, ))
# $(eval $(call build-one-prebuilt-binary, sample-two, , EXECUTABLES, hw, 64, ))
# $(eval $(call build-one-prebuilt-binary, sample-three, , EXECUTABLES, hw, both, init.freeme.fingerprint.ext))
#############################################################################
# CONTENT
#----------------------------------------------------------------------------
$(eval $(call build-one-prebuilt-binary, fingerprint.sunwave_ree, .so, SHARED_LIBRARIES, hw, both, vendor.sw.swfingerprint@1.0-service))
$(eval $(call build-one-prebuilt-binary, vendor.sw.swfingerprint@1.0, .so, SHARED_LIBRARIES, , both, ))
#$(eval $(call build-one-prebuilt-binary, fingerprint.default, .so, SHARED_LIBRARIES, hw, both, ))
$(eval $(call build-one-prebuilt-binary, vendor.sw.swfingerprint@1.0-impl, .so, SHARED_LIBRARIES, hw, 32, ))
$(eval $(call build-one-prebuilt-binary, vendor.sw.swfingerprint@1.0-service, , EXECUTABLES, hw, 32, ))

