## TYD Begin ################################################################
TYD_DEVICE_LCD_DENSITY = 480
TYD_SYS_VERSION = 9.0.0
TYD_SYS_LABEL = TYDOS
TYD_SYS_CHANNEL = DROIPCB
TYD_SYS_CUSTOMER = TYD
TYD_SYS_CUSTOMER_BRANCH = 1
# [Asia/Shanghai]
TYD_SYS_TIMEZONE = Asia/Shanghai
TYD_SYS_BLUETOOTH_NAME = ANDROID BT
TYD_SYS_WLAN_NAME = AndroidAP
TYD_SYS_MTP_PTP_NAME =
TYD_SYS_EXIF_NAME =

TYD_PRODUCT_INFO_MODEL = TYD
TYD_PRODUCT_INFO_BRAND = TYD
TYD_PRODUCT_INFO_NAME = TYD
TYD_PRODUCT_INFO_BOARD = TYD
TYD_PRODUCT_INFO_DEVICE = TYD
TYD_PRODUCT_INFO_MANUFACTURER = TYD
TYD_PRODUCT_INFO_BASEBAND_VERNO =
TYD_PRODUCT_INFO_KERNEL_VERNO =
TYD_PRODUCT_INFO_FLAVOR =
TYD_PRODUCT_INFO_HW_VERNO = TYD-r1
TYD_PRODUCT_INFO_SW_VERNO = TYD-9.0.0_r1
TYD_PRODUCT_INFO_SW_VERNO_INTERNAL =
# [factory|cts]
TYD_PRODUCT_FOR =
# [zh_Hans_CN|en_US]
TYD_PRODUCT_LOCALES =
TYD_PRODUCT_LOCALES_DEFAULT = en_US
TYD_PRODUCT_LOCALES_FILTER_OUT =

BOOT_LOGO = fhd
TYD_BOOT_ANIMATION_CUSTOMIZED = no
TYD_BOOT_AUDIO_SUPPORT = no
TYD_SHUT_ANIMATION_SUPPORT = no
TYD_SHUT_AUDIO_SUPPORT = no

TYD_WALLPAPERS_CUSTOMIZED = no

# @} ###########################################################################

# @{ TYD Feature ############################################################


# @} ###########################################################################

## TYD End ##################################################################

## Other Applications Begin ####################################################


## Other Applications End ######################################################
